package com.ironbrick.invent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.ironbrick.invent.db.MyDBHandler;

import java.util.HashMap;

public class EditInvention extends Activity {

    int inventionID;
    Invention inventionToEdit;
    EditText titleET, descET, weeklyRevET, weeklyCostET, monthlyGrowthET, sPrincipleET, sCostET;
    RadioGroup catButtonGroup;
    MyDBHandler dbHandler;
    int category;
    Spinner catSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_invention);
        // set a back button in action bar
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // grab the id from the intent
        inventionID = getIntent().getIntExtra("id", 0);
        // Get the invention to edit by the id that was passed in
        try {
            inventionToEdit = Inventions.getInventionByID(inventionID);
        } catch (Exception e) {
            System.out.println("Problem finding the invention with that ID in" +
                    this.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
        dbHandler = new MyDBHandler(this, null, null, 1);
        titleET = (EditText) findViewById(R.id.titleET);
        descET = (EditText) findViewById(R.id.descET);
        sPrincipleET = (EditText) findViewById(R.id.sPrincipleET);
        sCostET = (EditText) findViewById(R.id.sCostET);
        weeklyRevET = (EditText) findViewById(R.id.weeklyRevET);
        weeklyCostET = (EditText) findViewById(R.id.weeklyCostET);
        monthlyGrowthET = (EditText) findViewById(R.id.monthlyGrowthET);
        // Set default values
        titleET.setText(inventionToEdit.getName());
        descET.setText(inventionToEdit.getDescription());
        sPrincipleET.setText(String.valueOf(inventionToEdit.getStartingPrinciple()));
        sCostET.setText(String.valueOf(inventionToEdit.getStartingCost()));
        weeklyRevET.setText(String.valueOf(inventionToEdit.getWeeklyRev()));
        weeklyCostET.setText(String.valueOf(inventionToEdit.getWeeklyCost()));
        monthlyGrowthET.setText(String.valueOf(inventionToEdit.getGrowthRate()));
        // Set up spinner for category drop down
        catSP = (Spinner)findViewById(R.id.categorySP);
        HashMap<Integer, String> categories = dbHandler.getCategories();
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, categories.values().toArray(new String[categories.size()]));
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catSP.setAdapter(spArrayAdapter);

        // TODO Display current financial info

        // TODO Save updated versions of financial info
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_invention, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            // don't need to launch activity, because no changes were made if going back
            finish();
        } else if (id == R.id.edit_save_btn) {
            // Save the changes made
            saveData();
            // go back to display page
            goBack();
        }

        return super.onOptionsItemSelected(item);
    }

    // function to store all the changes made to the invention
    private boolean saveData() {
        String title, desc;
        int weeklyCost, weeklyRev, monthlyGrowth, startingCost, startingPrinciple;
        title = titleET.getText().toString();
        desc = descET.getText().toString();
        category = catSP.getSelectedItemPosition() + 1;
        startingPrinciple = Integer.parseInt(sPrincipleET.getText().toString());
        startingCost = Integer.parseInt(sCostET.getText().toString());
        weeklyCost = Integer.parseInt(weeklyCostET.getText().toString());
        weeklyRev = Integer.parseInt(weeklyRevET.getText().toString());
        monthlyGrowth = Integer.parseInt(monthlyGrowthET.getText().toString());

        // Creates a clone of the invention to edit, that will be used to
        // copy the new values over.
        inventionToEdit = new Invention(title, desc, category, startingPrinciple, startingCost,
                                        weeklyRev, weeklyCost, monthlyGrowth);
        inventionToEdit.setId(inventionID);
        // save the changes to the database
        dbHandler.updateInvention(inventionToEdit);
        // Get the updated list from the database and refresh the list
        Inventions.inventions = dbHandler.getInventions();
        return true;
    }

    public void onSaveButtonClick(View view) {
        saveData();
        goBack();
    }

    private void goBack() {
        // go back to display page
        Intent intent = new Intent(this,DisplayItem.class);
        // pass the id extra through to the display class
        intent.putExtra("id",getIntent().getIntExtra("id",0));
        startActivity(intent);
    }
}
