package com.ironbrick.invent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.ironbrick.invent.db.MyDBHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AddItem extends Activity {

    Button submit;
    EditText titleET;
    EditText descET;
    String title, desc;
    int category;
    Invention newInvention;
    //RadioGroup cat_group;
    Spinner catSP;
    MyDBHandler dbHandler;
    NumberPicker np;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add);
        // add back button to action bar
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // set up dbHandler for the class
        dbHandler = new MyDBHandler(this, null, null, 1);

        // find title edit text
        titleET = (EditText) findViewById(R.id.title_edit_text);
        descET = (EditText) findViewById(R.id.desc_edit_text);
        //cat_group = (RadioGroup) findViewById(R.id.cat_btn_group);
        catSP = (Spinner) findViewById(R.id.catSP);
        List<Integer> sortedKeys = new ArrayList<Integer>(MainActivity.catMap.keySet());
        Collections.sort(sortedKeys);
        ArrayList<String> titles = new ArrayList<String>();
        for (Integer key : sortedKeys) {
            // Then it is the 'all' category
            titles.add(MainActivity.catMap.get(key));
        }
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item,titles);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catSP.setAdapter(spArrayAdapter);
        // Set category to app by default, will be changed if button clicked
        category = 1;

        // Set up number picker for growth rate
        np = (NumberPicker) findViewById(R.id.growthPK);
        np.setMaxValue(20);
        np.setMinValue(0);
        np.setWrapSelectorWheel(true);
        np.setValue(5);

        // find button and attach listener to add new value
        submit = (Button) findViewById(R.id.submitBTN);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // First check if the title field is empty and set title value
                if (!titleET.getText().toString().equals("") && !descET.getText().toString().equals("")) {
                    addInventionFromData();
                    // return to main activity
                    Intent returnIntent = new Intent(AddItem.this, MainActivity.class);
                    startActivity(returnIntent);
                    finish();
                } else {
                    // Display toast to enter a title
                    Toast.makeText(getApplicationContext(), "Empty Field", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addInventionFromData() {
        // get the text field data
        title = titleET.getText().toString();
        desc = descET.getText().toString();
        catSP.getSelectedItem();
        category = catSP.getSelectedItemPosition() + 1;
        // Financial Information
        EditText weeklyRevET = (EditText) findViewById(R.id.wRevET);
        EditText weeklyCostET = (EditText) findViewById(R.id.wCostET);
        EditText startingPrincipleET = (EditText) findViewById(R.id.sPrincipleET);
        EditText startingCostET = (EditText) findViewById(R.id.sCostET);
        int weeklyRev = weeklyRevET.getText().toString().isEmpty() ? 0 :
                Integer.parseInt(weeklyRevET.getText().toString());
        int weeklyCost = weeklyCostET.getText().toString().isEmpty() ? 0 :
                Integer.parseInt(weeklyCostET.getText().toString());
        int startingPrinciple = startingPrincipleET.getText().toString().isEmpty() ? 0 :
                Integer.parseInt(startingPrincipleET.getText().toString());
        int startingCost = startingCostET.getText().toString().isEmpty() ? 0 :
                Integer.parseInt(startingCostET.getText().toString());
        int growthRate = np.getValue();

        // create a new invention object
        newInvention = new Invention(title, desc, category, startingPrinciple, startingCost, weeklyRev, weeklyCost,
                growthRate);
        // add the new element to the list - this may be redundant now because list is
        // fetched from database onCreate of main method
        Inventions.inventions.add(newInvention);
        // Add the new invention to the database
        dbHandler.addInvention(newInvention);
    }

    @Override
    public void onBackPressed() {
        if (!titleET.getText().toString().equals("") && !descET.getText().toString().equals("")) {
            addInventionFromData();
        }
        Intent returnIntent = new Intent(AddItem.this, MainActivity.class);
        startActivity(returnIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            if (!titleET.getText().toString().equals("") && !descET.getText().toString().equals("")) {
                addInventionFromData();
            }
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
