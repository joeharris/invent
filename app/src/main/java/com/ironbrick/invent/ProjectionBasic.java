package com.ironbrick.invent;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.ironbrick.invent.db.MyDBHandler;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;

public class ProjectionBasic extends Activity {

    MyDBHandler dbHandler;
    Invention selected;
    Invention secondInvention;
    ArrayList<Double> netEarnings;
    LineChart chart;
    SeekBar sb;
    ArrayList<LineDataSet> dataSets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.projection_basic);
        // makes icon clickable
        getActionBar().setDisplayHomeAsUpEnabled(true);
        dbHandler = new MyDBHandler(this, null, null, 1);
        // Get the selected item from the intent-passed id
        selected = Inventions.getInventionByID(getIntent().getIntExtra("id", 0));
        secondInvention = null;
        // Set up the seek bar at the bottom
        sb = (SeekBar) findViewById(R.id.monthsToProjectSB);
        sb.setMax(30);
        // Set text view values
        TextView titleTV = (TextView)findViewById(R.id.titleTV);
        TextView idTV = (TextView)findViewById(R.id.idTV);
        titleTV.setText(selected.getName());
        idTV.setText(String.valueOf(selected.getId()));
        chart = (LineChart) findViewById(R.id.projectionChart);
        populateGraph(selected, secondInvention);

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                populateGraph(selected, secondInvention);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public float calculateCash (Invention selected, int month) {
        final float monthlyRev = selected.getWeeklyRev()*4;
        final float monthlyCost = selected.getWeeklyCost()*4;
        final float startingPrinciple = selected.getStartingPrinciple();
        final float startingCost = selected.getStartingCost();
        final float monthlyGrowthRate = (float)selected.getGrowthRate();
        float initialCash = startingPrinciple - startingCost;
        float growthCoefficient = (float) Math.pow((1 + monthlyGrowthRate / 100), month);
        float dampingFactor = (float)Math.pow(0.95, month);
        float cash = initialCash + dampingFactor*(monthlyRev*growthCoefficient -
                monthlyCost*growthCoefficient/2);

        return cash;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_projection_basic, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.compare) {
            createDialogToSelectComparison();
            return true;
        } else if (id == android.R.id.home) {
            // go back to display page
            goBack();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void goBack() {
        // go back to display page
        Intent intent = new Intent(this,DisplayItem.class);
        // pass the id extra through to the display class
        intent.putExtra("id",getIntent().getIntExtra("id",0));
        startActivity(intent);
    }

    private void createDialogToSelectComparison() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.comparison_dialog_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        // Set dialog message
        alertDialogBuilder.setTitle("Select an invention to compare against: ");
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        final Spinner mSpinner = (Spinner) promptsView.findViewById(R.id.compareSpinner);
        final Button mButton = (Button) promptsView.findViewById(R.id.compareButton);
        ArrayList<String> inventionTitles = new ArrayList<String>();
        // Keep a matching array of ids that line up for later fetching
        final ArrayList<Integer> inventionIds = new ArrayList<Integer>();
        for (Invention i : Inventions.getList()) {
            inventionTitles.add(i.getName());
            inventionIds.add(i.getId());
        }
        ArrayAdapter<String> spArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,inventionTitles);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(spArrayAdapter);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int secondID = inventionIds.get(mSpinner.getSelectedItemPosition());
                Log.i(String.valueOf(mSpinner.getSelectedItem()), "selected " + String.valueOf(secondID));
                if (selected.getId() != secondID) {
                    secondInvention = Inventions.getInventionByID(
                            inventionIds.get(mSpinner.getSelectedItemPosition()));
                    alertDialog.cancel();
                    populateGraph(selected,secondInvention);
                } else {
                    Log.i("Selected: ", "same invention");
                }

            }
        });

        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(true);
    }

    private void populateGraph(Invention selected, Invention secondInvention) {
        ArrayList<Entry> lineEntries = new ArrayList<Entry>();
        ArrayList<String> xLabels = new ArrayList<String>();
        ArrayList<Entry> lineEntries2 = new ArrayList<Entry>();
        // Generates the projection for n months and stores in array
        for (int n = 0; n < 10+sb.getProgress(); n++) {
            float newValue = calculateCash(selected,n);
            lineEntries.add(new Entry(newValue, n));
            if (secondInvention != null) {
                lineEntries2.add(new Entry(calculateCash(secondInvention,n),n));
            }
            xLabels.add(Integer.toString(n) + "M");
        }
        LineDataSet setLine = new LineDataSet(lineEntries, "Graph yis");
        setLine.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSets = new ArrayList<LineDataSet>();
        dataSets.add(setLine);
        if (secondInvention != null) {
            LineDataSet setLine2 = new LineDataSet(lineEntries2, "Graph 2 yis");
            setLine2.setAxisDependency(YAxis.AxisDependency.LEFT);
            setLine2.setColor(getResources().getColor(android.R.color.black));
            dataSets.add(setLine2);
        }

        LineData data = new LineData(xLabels, dataSets);
        chart.setData(data);
        chart.setMaxVisibleValueCount(0);
        chart.invalidate();
    }


}
