package com.ironbrick.invent;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbrick.invent.db.MyDBHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MainActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private ListView mainList;
    TextView nothingHereText;
    MyDBHandler dbHandler;
    Inventions inventionList;
    public static HashMap<Integer, String> catMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = getTitle();
        // get the list view object
        mainList = (ListView) findViewById(R.id.main_list);

        // set up the list to handle the long press
        registerForContextMenu(mainList);

        // setup dbhandler for the class
        dbHandler = new MyDBHandler(this, null, null, 5);
        inventionList = new Inventions(dbHandler.getInventions());

        catMap = dbHandler.getCategories();
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        // reverse the list so that you can print it in a stack order
        Collections.reverse(Inventions.inventions);


        // handle for when there are no entries
        nothingHereText = (TextView) findViewById(R.id.nothing_here_text);
        if (dbHandler.isInventionsEmpty()) {
            nothingHereText.setVisibility(View.VISIBLE);
            nothingHereText.setText("There appears to be nothing here... Add something now!");
        } else {
            nothingHereText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

        // Set the title for the context menu to the item itself
        String title = inventionList.getInventionByPosition(info.position).getName();
        menu.setHeaderTitle(title);

        // Make the menu for the pop up
        menu.add(Menu.NONE, 0, 0, "Edit");
        menu.add(Menu.NONE, 1, 1, "Duplicate");
        menu.add(Menu.NONE, 2, 2, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            // Edit case
            case 0:
                // go to edit page
                Intent intent = new Intent(this,EditInvention.class);
                // pass the id extra through to the edit class
                intent.putExtra("id",getIntent().getIntExtra("id",inventionList.getInventionByPosition(info.position).getId()));
                startActivity(intent);
                return true;
            // Duplicate case
            case 1:
                // create a new invention object
                Invention i = inventionList.getInventionByPosition(info.position);
                Invention newInvention = new Invention(i.getName() + " Copy", i.getDescription(),
                        i.getCategory(), i.getStartingPrinciple(), i.getStartingCost(),
                        i.getWeeklyRev(), i.getWeeklyCost(), i.getGrowthRate());
                // add the new element to the list - this may be redundant now because list is
                // fetched from database onCreate of main method
                inventionList.getList().add(newInvention);
                // Add the new invention to the database
                dbHandler.addInvention(newInvention);
                return true;
            // Delete case
            case 2:
                dbHandler.deleteInvention(inventionList.getInventionByPosition(info.position).getId());
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        mTitle = catMap.get(number);
        populateListView(number);
    }

    // populate the list view with the correct values
    private void populateListView(int category) {
        // gets the correct list
        final ArrayList<Invention> list = Inventions.getCategoryList(category);
        // sets the array adapter to the names from the gotten list
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Inventions.getNames(list));
        mainList.setAdapter(arrayAdapter);
        // set up a listener to handle item presses
        mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Invention selectedItem = list.get(position);
                // Move to display activity and send selected id
                Intent intent = new Intent(MainActivity.this, DisplayItem.class);
                intent.putExtra("id", selectedItem.getId());
                startActivity(intent);
            }
        });
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar == null) {
            throw new AssertionError();
        } else {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(mTitle);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView;
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
