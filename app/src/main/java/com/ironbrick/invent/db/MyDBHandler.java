package com.ironbrick.invent.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ironbrick.invent.Invention;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Joe on 8/18/2015.
 */
public class MyDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "inventions.db";
    public static final String TABLE_INVENTIONS = "inventions";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CAT = "category";
    public static final String COLUMN_DESC = "description";
    private static final String COLUMN_DATE = "date";
    public static final String COLUMN_WREV = "weekly_revenue";
    public static final String COLUMN_WCOST = "weekly_cost";
    public static final String COLUMN_MGROWTH= "monthly_growth";
    public static final String COLUMN_SPRINCIPLE = "start_principle";
    public static final String COLUMN_SCOST = "start_cost";

    public static final String TABLE_CATEGORIES = "id_categories";


    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // query to make a new table
        String query = "CREATE TABLE " + TABLE_INVENTIONS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_DESC + " TEXT, " +
                COLUMN_CAT + " INTEGER, " +
                COLUMN_DATE + " TEXT, " +
                COLUMN_MGROWTH + " INTEGER, " +
                COLUMN_SPRINCIPLE + " INTEGER, " +
                COLUMN_SCOST + " INTEGER, " +
                COLUMN_WREV + " INTEGER, " +
                COLUMN_WCOST + " INTEGER " +");";
        db.execSQL(query);
        // query to make a new table for id->category
        query = "CREATE TABLE " + TABLE_CATEGORIES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_CAT + " TEXT );";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 'refresh' the table with the new properties
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        onCreate(db);
    }

    // Add a new row to database
    public void addInvention(Invention invention) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, invention.getName());
        values.put(COLUMN_DESC, invention.getDescription());
        values.put(COLUMN_CAT, invention.getCategory());
        values.put(COLUMN_DATE, invention.getDate().toString());
        values.put(COLUMN_SPRINCIPLE, invention.getStartingPrinciple());
        values.put(COLUMN_SCOST, invention.getStartingCost());
        values.put(COLUMN_WREV, invention.getWeeklyRev());
        values.put(COLUMN_WCOST, invention.getWeeklyCost());
        values.put(COLUMN_MGROWTH, invention.getGrowthRate());
        SQLiteDatabase db = getWritableDatabase();
        // insert the value set into the table
        db.insert(TABLE_INVENTIONS, null, values);
        db.close();//close the database now that we're done
    }

    // Delete an invention
    public void deleteInvention(int id) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_INVENTIONS + " WHERE " + COLUMN_ID + "=\"" + id + "\";");
    }

    // Convert the database info into an arraylist to be used in the app
    public ArrayList<Invention> getInventions() {
        ArrayList<Invention> list = new ArrayList<Invention>();
        // get the database to work with
        SQLiteDatabase db = getWritableDatabase();
        // means select all rows and all columns in those rows
        String query = "SELECT * FROM " + TABLE_INVENTIONS;
        // Create a cursor to move through the table
        Cursor c = db.rawQuery(query, null);
        // Move the first row of table
        c.moveToFirst();

        // Move through row by row
        while(!c.isAfterLast()) {
            // blank invention object
            Invention inv = new Invention(null,null,-1,-1,-1,-1,-1,-1);
            // If there are valid values saved for the fields, collect them and store in blank
            // invention object
            if(c.getString(c.getColumnIndex(COLUMN_ID)) != null) {
                inv.setId(c.getInt(c.getColumnIndex(COLUMN_ID)));
            }
            if(c.getString(c.getColumnIndex(COLUMN_NAME)) != null) {
                inv.setName(c.getString(c.getColumnIndex(COLUMN_NAME)));
            }
            if(c.getString(c.getColumnIndex(COLUMN_DESC)) != null) {
                inv.setDescription(c.getString(c.getColumnIndex(COLUMN_DESC)));
            }
            if(c.getString(c.getColumnIndex(COLUMN_CAT)) != null) {
                inv.setCategory(c.getInt(c.getColumnIndex(COLUMN_CAT)));
            }
            if(c.getString(c.getColumnIndex(COLUMN_DATE)) != null) {
                inv.setDate(c.getString(c.getColumnIndex(COLUMN_DATE)));
            }
            if(c.getString(c.getColumnIndex(COLUMN_SPRINCIPLE)) != null) {
                inv.setStartingPrinciple(c.getInt(c.getColumnIndex(COLUMN_SPRINCIPLE)));
            }
            if(c.getString(c.getColumnIndex(COLUMN_SCOST)) != null) {
                inv.setStartingCost(c.getInt(c.getColumnIndex(COLUMN_SCOST)));
            }
            if (c.getString(c.getColumnIndex(COLUMN_WREV)) != null) {
                inv.setWeeklyRev(c.getInt(c.getColumnIndex(COLUMN_WREV)));
            }
            if (c.getString(c.getColumnIndex(COLUMN_WCOST)) != null) {
                inv.setWeeklyCost(c.getInt(c.getColumnIndex(COLUMN_WCOST)));
            }
            if(c.getString(c.getColumnIndex(COLUMN_MGROWTH)) != null) {
                inv.setGrowthRate(c.getInt(c.getColumnIndex(COLUMN_MGROWTH)));
            }
            // add the newly built invention object from this row
            list.add(inv);
            c.moveToNext();
        }
        db.close();

        return list;
    }

    // Updates the invention given
    public void updateInvention (Invention invention) {
        String query = "UPDATE " + TABLE_INVENTIONS + " SET " + COLUMN_NAME +
                "=\"" + invention.getName() + "\"," + COLUMN_DESC + "=\"" + invention.getDescription() +
                "\"," + COLUMN_CAT + "=\"" + invention.getCategory() + "\", " + COLUMN_SPRINCIPLE + "=\"" +
                invention.getStartingPrinciple() + "\"," + COLUMN_SCOST + "=\"" + invention.getStartingCost()
                + "\"," + COLUMN_WREV + "=\"" + invention.getWeeklyRev() + "\", " + COLUMN_WCOST + "=\"" +
                invention.getWeeklyCost() + "\", " + COLUMN_MGROWTH + "=\"" + invention.getGrowthRate() +
                "\" WHERE " + COLUMN_ID + "=" + invention.getId()+";";
        // get the database to work with
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query);
    }

    // Counts the number of entries
    public int numEntriesInInventions() {
        int num = 0;
        // get the database to work with
        SQLiteDatabase db = getWritableDatabase();
        // means select all rows and all columns in those rows
        String query = "SELECT * FROM " + TABLE_INVENTIONS;
        // Create a cursor to move through the table
        Cursor c = db.rawQuery(query, null);
        // Move the first row of table
        c.moveToFirst();
        // Move through row by row
        while(!c.isAfterLast()) {
            num++;
            // add the newly built invention object from this row
            c.moveToNext();
        }
        db.close();
        return num;
    }

    // Checks if inventions table is empty
    public boolean isInventionsEmpty() {
        boolean isEmpty;
        if (numEntriesInInventions() == 0) {
            isEmpty = true;
        } else {
            isEmpty = false;
        }
        return isEmpty;
    }

    // Checks if the category exists in the table already
    public boolean isCategoryAlreadyInTable(String category) {
        SQLiteDatabase db = getWritableDatabase();
        boolean returnVal = true; // init to true
        String query = "SELECT * FROM " + TABLE_CATEGORIES + " WHERE " +
                COLUMN_CAT + "='" + category + "'";
        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        // Loop through, set false if find category in there
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_CAT)) == category) {
                // Then it already exists in db
                returnVal = false;
                break;
            }
            c.moveToNext();
        }
        return returnVal;
    }

    // Will add a category into the table if it is not
    // already there. Return true if successful
    public boolean addNewCategory (String category) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_CAT, category);
        //if (!isCategoryAlreadyInTable(category)) {
        db.insert(TABLE_CATEGORIES, null, values);
        db.close();
        return true;
        //}


    }

    public HashMap<Integer, String> getCategories () {
        HashMap<Integer, String> catMap = new HashMap<Integer, String>();
        String query = "SELECT * FROM " + TABLE_CATEGORIES;
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            catMap.put(c.getInt(c.getColumnIndex(COLUMN_ID)),
                    c.getString(c.getColumnIndex(COLUMN_CAT)));
            c.moveToNext();
        }
        System.out.println(catMap);
        catMap.put(catMap.size() + 1,"All");
        return catMap;
    }

}
