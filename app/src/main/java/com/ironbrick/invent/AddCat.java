package com.ironbrick.invent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ironbrick.invent.db.MyDBHandler;

/**
 * Created by Joe on 24/12/15.
 */
public class AddCat extends Activity {

    EditText addCatET;
    Button addCatBTN;
    MyDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addcat);
        // add back button to action bar
        getActionBar().setDisplayHomeAsUpEnabled(true);
        dbHandler = new MyDBHandler(this, null, null, 1);

        addCatET = (EditText)findViewById(R.id.addCatET);
        addCatBTN = (Button)findViewById(R.id.addCatBTN);

        addCatBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addCatET.getText().toString() != "") {
                    String newCat = addCatET.getText().toString();
                    dbHandler.addNewCategory(newCat);
                    Intent intent = new Intent(AddCat.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return true;

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(AddCat.this, MainActivity.class);
        startActivity(returnIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
