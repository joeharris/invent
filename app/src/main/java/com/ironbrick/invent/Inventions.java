package com.ironbrick.invent;

import com.ironbrick.invent.db.MyDBHandler;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Joe on 8/16/2015.
 */
public class Inventions {
    public static ArrayList<Invention> inventions;

    public Inventions (ArrayList<Invention> inventions) {
        this.inventions = inventions;
    }

    public Invention getInventionByPosition (int position) {
        return inventions.get(position);
    }

    public static Invention getInventionByID(int id) {
        // set to first invention by default
        Invention inv = inventions.get(0);
        // find invention with the given id and return it
        for (Invention i : inventions) {
            if (i.getId() == id) {

                inv = i;
            }
        }
        return inv;
    }

    // gets a list of all names in a given list of inventions
    public static ArrayList<String> getNames(ArrayList<Invention> list) {
        ArrayList<String> names = new ArrayList<String>();
        for (Invention i : list) {
            names.add(i.getName());
        }
        return names;
    }

    // given a category it will return a list of only the relevant category of inventions
    public static ArrayList<Invention> getCategoryList(int category) {
        ArrayList<Invention> relevantInventions = new ArrayList<Invention>();
        for (Invention i : inventions) {
            if (i.getCategory() == category || category == MainActivity.catMap.size()) {
                relevantInventions.add(i);
            }
        }
        return relevantInventions;
    }

    public static ArrayList<Invention> getList () {
        return inventions;
    }

    public static void setInventions(ArrayList<Invention> inventions) {
        Inventions.inventions = inventions;
    }
}
