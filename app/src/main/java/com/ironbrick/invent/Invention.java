package com.ironbrick.invent;

import android.text.format.DateUtils;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Joe on 8/16/2015.
 */
public class Invention {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDate(Date date) {
        this.date = date.toString();
    }

    public int getWeeklyCost() {
        return weeklyCost;
    }

    public void setWeeklyCost(int weeklyCost) {
        this.weeklyCost = weeklyCost;
    }

    public int getWeeklyRev() {
        return weeklyRev;
    }

    public void setWeeklyRev(int weeklyRev) {
        this.weeklyRev = weeklyRev;
    }

    public int getStartingPrinciple() {
        return startingPrinciple;
    }

    public void setStartingPrinciple(int startingPrinciple) {
        this.startingPrinciple = startingPrinciple;
    }

    public int getStartingCost() {
        return startingCost;
    }

    public void setStartingCost(int startingCost) {
        this.startingCost = startingCost;
    }

    public int getGrowthRate() {
        return growthRate;
    }

    public void setGrowthRate(int growthRate) {
        this.growthRate = growthRate;
    }

    public Invention (String title, String description, int category, int startingPrinciple,
                      int startingCost, int weeklyRev, int weeklyCost, int growthRate) {
        this.name = title;
        this.description = description;
        this.category = category;
        Date date = new Date();
        this.date = dateFormat.format(date).toString();
        this.weeklyRev = weeklyRev;
        this.weeklyCost = weeklyCost;
        this.growthRate = growthRate;
        this.startingPrinciple = startingPrinciple;
        this.startingCost = startingCost;
    }

    // Basic info
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    String name;
    String description;
    int id;
    int category;
    String date;

    // Financial Info
    private int weeklyCost;
    private int weeklyRev;
    private int startingPrinciple;
    private int startingCost;
    private int growthRate;


}
