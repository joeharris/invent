package com.ironbrick.invent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbrick.invent.db.MyDBHandler;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DisplayItem extends Activity {

    TextView titleTV, descTV, catTV, dateTV, idTV, weeklyRevTV, weeklyCostTV, mGrowthTV, sPrincipleTV, sCostTV;
    Invention selected;
    Button editBtn, deleteBtn;
    MyDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_item);
        // makes icon clickable
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // Set up handler
        dbHandler = new MyDBHandler(this, null, null, 1);
        // Get the selected item from the intent-passed id
        selected = Inventions.getInventionByID(getIntent().getIntExtra("id", 0));
        // find text views
        titleTV   =  (TextView) findViewById(R.id.titleTV);
        descTV    =  (TextView) findViewById(R.id.descTV);
        catTV     =  (TextView) findViewById(R.id.catTV);
        dateTV    =  (TextView) findViewById(R.id.date_text_view);
        idTV      =  (TextView) findViewById(R.id.id_text_view);
        sPrincipleTV = (TextView) findViewById(R.id.sPrincipleTV);
        sCostTV = (TextView) findViewById(R.id.sCostTV);
        weeklyRevTV  = (TextView) findViewById(R.id.weeklyRevTV);
        weeklyCostTV = (TextView) findViewById(R.id.weeklyCostTV);
        mGrowthTV =  (TextView) findViewById(R.id.growthTV);
        editBtn   =  (Button)   findViewById(R.id.edit_button);
        deleteBtn =  (Button)   findViewById(R.id.delete_button);
        // sets the text values if they are not null
        titleTV.setText(selected.getName() == null ? "BLANK" : selected.getName());
        descTV.setText(selected.getDescription() == null ? "BLANK" : selected.getDescription());
        sPrincipleTV.setText("$"+String.valueOf(selected.getStartingPrinciple()));
        sCostTV.setText("$"+String.valueOf(selected.getStartingCost()));
        weeklyRevTV.setText("$"+String.valueOf(selected.getWeeklyRev()));
        weeklyCostTV.setText("$"+String.valueOf(selected.getWeeklyCost()));
        mGrowthTV.setText(String.valueOf(selected.getGrowthRate())+"%");
        catTV.setText(String.valueOf(MainActivity.catMap.get(selected.getCategory())));
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        // parse the string back into the right format
        dateTV.setText("Date added: " + selected.getDate());
        idTV.setText(String.valueOf(selected.getId()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            // go back to main list
            startActivity(new Intent(this,MainActivity.class));
            finish();
        } else if (id == R.id.edit_item) {
            editItem();
        }

        return super.onOptionsItemSelected(item);
    }



    // Deletes the displayed invention
    public void deleteButtonClicked(View view) {
        dbHandler.deleteInvention(selected.getId());
        startActivity(new Intent(this, MainActivity.class));
    }

    // Goes to edit the displayed invention
    public void editButtonClicked(View view) {
        editItem();
    }

    // Moves app to edit screen
    private void editItem() {
        // go to edit page
        Intent intent = new Intent(this,EditInvention.class);
        // pass the id extra through to the edit class
        intent.putExtra("id",getIntent().getIntExtra("id",0));
        startActivity(intent);
    }

    // Moves to project activity
    public void projectButtonClicked(View view) {
        // go to edit page
        Intent intent = new Intent(this,ProjectionBasic.class);
        // pass the id extra through to the edit class
        intent.putExtra("id",getIntent().getIntExtra("id",0));
        startActivity(intent);
    }
}
