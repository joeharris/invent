# What is *Invent* #

Repository holding the development of **Invent**. An application to track, project and compare your business ideas in order to select the most profitable one for you to pour your time into - judging based on time commitments, your passions and obviously, profitability and potential for growth.

### How do I get set up? ###

* Download the application from the Google Play marketplace
* Open it
* Use it
* ?
* Profit

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact